﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcSchoolWebApp.Data
{
    public class PublishResultClass
    {
        public string classid { get; set; }
        public string classes { get; set; }
        public string sectionid { get; set; }
        public string section { get; set; }
        public string subjectid { get; set; }
        public string subject { get; set; }
        public string status { get; set; }
        public string begdate { get; set; }
    }
}