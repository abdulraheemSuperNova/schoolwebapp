﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcSchoolWebApp.Models
{
    public class JQGridModel
    {
        public string studentId { get; set; }
        public string studentName { get; set; }
        public string fatherName { get; set; }
        public string status { get; set; }
        public string totalmarkstxt { get; set; }
        public string resulttype { get; set; }

        public  string totalmarks { get; set; }
        public string examinationmarks { get; set; }
        //public string projectmarks { get; set; }
        //public string testmarks { get; set; }
        //public string oralmarks { get; set; }
        public string assignmentmarks { get; set; }
        public string obtainedmarks { get; set; }
        public string quiz { get; set; }
        public int serialNo { get; set; }
        public string fileName { get; set; }
        public string date { get; set; }
        public string viewButton { get; set; }
        public string downloadButton { get; set; }
        public string campus { get; set; }
        public string classes { get; set; }
        public string section { get; set; }
        public string subject { get; set; }
        public string classteach { get; set; }
        public string total { get; set; }
        public string boys { get; set; }
        public string girls { get; set; }
        public string exammarks { get; set; }
        public string projectmarks { get; set; }
        public string testmarks { get; set; }
        public string oralmarks { get; set; }
        public string p1marks { get; set; }
        public string p2marks { get; set; }
        public string p3marks { get; set; }
        public string assignmarks { get; set; }
        public double percentage { get; set; }
        public string assignppt { get; set; }
        public string gk { get; set; }
        public string behave { get; set; }
        public string discp { get; set; }
        public string clean { get; set; }
        public int totalstrength { get; set; }
        public int totalboys { get; set; }
        public int totalgirls { get; set; }
        public string Caption { get; set; }
        public string user_role { get; set; }
        public string reading { get; set; }
        public string rhymes { get; set; }
        public string recog_letter { get; set; }
        public string conversation { get; set; }
        public string vocabulary { get; set; }
        public string pronunciation { get; set; }
        public string responsivess { get; set; }
        public string writing { get; set; }
        public string tracing { get; set; }
        public string form_letter { get; set; }
        public string assocsounds { get; set; }
        public string joindodge { get; set; }
        public string matching { get; set; }
        public string dictation { get; set; }
        public string boardactivity { get; set; }
        public int totalnoofstd { get; set; }
        public string day { get; set; }
        public string total_present { get; set; }
        public string total_absent { get; set; }
        public string available { get; set; }
        public string occupied { get; set; }
        public int totseats { get; set; }
        public int totavaible { get; set; }
        public int totoccupied { get; set; }
        public string compliance { get; set; }
        public string task { get; set; }
        public string subjectnames { get; set; }
        public string variablenames { get; set; }
        public int count { get; set; }
        public string teachercom { get; set; }
        public string campusid { get; set; }
        public string classid { get; set; }
        public string sectionid { get; set; }
        public string subjectid { get; set; }
    }
}