﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcSchoolWebApp.Models
{
    public class ReportParamCls
    {
        public string Campus { get; set; }
        public string Class { get; set; }
        public string Section { get; set; }
        public string StudentName { get; set; }
        //public string Subject { get; set; }

        //public string ReportId { get; set; }
        public string Session { get; set; }

        public string ModuleID { get; set; }

        public string ReportID { get; set; }


    }
}