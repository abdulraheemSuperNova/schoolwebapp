﻿





//Bar Chart Average Test Marks
var ctx = document.getElementById('test-result-chart').getContext('2d');
var myChart = new Chart(ctx, {

    type: 'bar',

    data: {
        labels: ['Class1', 'Class2', 'Class3', 'Class4', 'Class5', 'Class6', 'Class7', 'Class8'],

        datasets: [
        {
            label: 'Monthly Test Result',
            data: [80, 85, 75, 90, 78, 83, 88, 76],
            backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f", "#a22615", "#ffcd34", "#0f5b78", "#f36f13", "#958c3d"],
        },

        ]
    },

});

//Bar Chart Average Test Marks


//Doughnut-chart No of Admission per Campuses
new Chart(document.getElementById("admission-chart"), {
    type: 'doughnut',
    data: {
        labels: ["Campus-A", "Campus-B", "Campus-C"],
        datasets: [
          {
              label: "",
              backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9"],
              data: [365, 325, 360]
          }
        ]
    },
    options: {
        title: {
            display: true,
            responsive: true,
            maintainAspectRatio: false,
            text: 'Number of Admissions in year 2017'
        }
    }
});

//Doughnut-chart No of Admission per Campuses




// Revenue Chart Per Month and Campuses

//var ctx = document.getElementById('revenue-chart').getContext('2d');

//var myChart = new Chart(ctx, {
//    type: 'bar',
//    data: {
//        labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December", ],
//        datasets: [
//          {
//              label: "Campus-A",
//              backgroundColor: "#33a02c",
//              data: [22000, 2400, 26700, 23500, 28000, 32000, 27600, 29800, 28000, 31000, 27600, 29800 ]
//          },
//          {
//              label: "Campus-B",
//              backgroundColor: "#dd4b39",
//              data: [21500, 23200, 25600, 27000, 28500, 29650, 24600, 27800, 28500, 27480, 24800, 23900]
//          },

//          {
//              label: "Campus-C",
//              backgroundColor: "#00c0ef",
//              data: [24050, 26800, 24600, 29200, 27200, 24150, 23900, 25500, 24600, 23900, 28700, 26500]
//          },
//        ]
//    },

//    options: {
//        title: {
//            display: true,
//            responsive: true,

//            text: 'Revenue Stats by Campuses'
//        }
//    }

//});

// Revenue Chart Per Month and Campuses



// Revenue Chart by each Campuses
var ctx = document.getElementById('revenue-chart').getContext('2d');
var myChart = new Chart(ctx, {

    type: 'line',

    data: {
        labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],

        datasets: [
        {
            label: 'Revenue by Campuses',
            data: [24050, 26800, 24600, 29200, 27200, 24150, 23900, 25500, 24600, 23900, 28700, 26500],
            //backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f", "#a22615", "#ffcd34", "#0f5b78", "#f36f13", "#958c3d", "#3e95cd", "#8e5ea2", "#3cba9f", "#0f5b78"],
            fillColor: "rgba(220,220,220,0.2)",
            strokeColor: "rgba(220,220,220,1)",
            pointColor: "rgba(220,220,220,1)",
            pointStrokeColor: "#fff",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(220,220,220,1)",
        },

        ]
    },

});

// Revenue Chart by each Campuses


new Chart(document.getElementById("no-of-student-chart"), {
    type: 'pie',
    data: {
        labels: ["Campus-A", "Campus-B", "Campus-C", "Campus-D", "Campus-E"],
        datasets: [
          {
              label: "",
              backgroundColor: ["#3e95cd", "#8e5ea2", "#3cba9f", "#e8c3b9", "#a22615"],
              data: [800, 650, 600, 735, 755]
          }
        ]
    },
    options: {
        title: {
            display: true,
            responsive: true,
            maintainAspectRatio: false,
            text: 'Number of Students in a Campus'
        }
    }
});