﻿

$(document).ready(function () {


    // Examle data for jqGrid
    var mydata = [
        { id: "1", filename: "Filename1", date:'11-07-2016', status: "view / download" },
        { id: "2", filename: "Filename2", date:'11-07-2016', status: "view / download" },
        { id: "3", filename: "Filename3", date:'11-07-2016', status: "view / download" },
        { id: "4", filename: "Filename4", date:'11-07-2016', status: "view / download" },
        { id: "5", filename: "Filename5", date:'11-07-2016', status: "view / download" },
        { id: "6", filename: "Filename6", date:'11-07-2016', status: "view / download" },

    ];

    // Configuration for jqGrid Example 1


    // Configuration for jqGrid Example 2
    $("#table_list_down").jqGrid({
        data: mydata,
        datatype: "local",
        height: 450,
        autowidth: true,
       
        rowNum: 20,
        rowList: [10, 20, 30],
        colNames: ['S No', 'File Name', 'Date' , 'Action'],
        colModel: [
            //width: "350", resizable: false, editable: false, align: 'center'
            { name: 'id', index: 'id', editable: false, width: 100, align: 'left', resizable: false, sorttype: "int", search: true },
            { name: 'filename', index: 'filename', editable: false, width: 700, align: 'left', resizable: false, search: true },
            //{ name: 'status', index: 'status', editable: false, width: 50, align: 'left', resizable: false}
            { name: 'date', index: 'Date', editable: false, width: 80, align: 'left', resizable: false, sorttype: "int", search: true },

            { name: 'status', index: 'status', editable: false, width: 180, align: 'left', resizable: false, sortable: false },


        ],

        gridComplete: function () {
            var ids = jQuery("#table_list_down").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var cl = ids[i];
                //class='btn btn-xs btn-info'
                view = "<input style='height:18px;width:75px;' type='button' value='View' class='btn btn-xs btn-primary'>";
                download = "&nbsp;<input style='height:18px;width:75px;' type='button' class='btn btn-xs btn-primary' value='Download' />";

                jQuery("#table_list_down").setRowData(ids[i], { status: view + download })
            }
        },

        pager: "#pager_list_down",
        viewrecords: true,
        //caption: "Quiz",
        add: true,
        edit: true,
        addtext: 'Add',
        edittext: 'Edit',
        hidegrid: false,
    shrinkToFit:false
    });

    // Add selection
    //$("#table_list_down").setSelection(4, true);


    // Setup buttons
    $("#table_list_down").jqGrid('navGrid', '#pager_list_down',
            { edit: true, add: true, del: true, search: true },
            { height: 200, reloadAfterSubmit: true }
    );

    // Add responsive to jqGrid
    $(window).bind('resize', function () {
        var width = $('.jqGrid_wrapper_down').width();

        $('#table_list_down').setGridWidth(width,false);
    });

    $("#download").hide();

    $("#table_list_down").jqGrid('setGridState', 'hidden');//or 'hidden' 

    $("#btnviewdown").click(function (e) {
        if ($("#downloadform").valid()) {
            $("#download").show();
            $("#table_list_down").jqGrid('setGridState', 'visible');//or 'hidden' 
        }
        return false;
    });

    $("#downloadform").validate({
        errorClass: "my-error-class",


        rules: {
            txtSubject: {
                required: true,
            },
            txtSection: {
                required: true,
            },
          
            txtClass: {
                required: true,
            },
            txtCampus: {
                required: true,
            }
        },
        messages: {
            txtSubject: {
                required: "",
            },
            txtSection: {
                required: ""
            },
            txtName: {
                required: ""
            },
            txtClass: {
                required: ""
            },
            txtCampus: {
                required: ""
            }
        }



    });

});



