﻿$(document).ready(function () {
    "use strict";
    // Examle data for jqGrid
    var mydata = [
        { date: "03-July-2017", day: "Monday", status: "Present" },
        { date: "04-July-2017", day: "Tuesday", status: "Absent" },
        { date: "05-July-2017", day: "Wednesday", status: "Absent" },
        { date: "06-July-2017", day: "Thursday", status: "Present" },
        { date: "07-July-2017", day: "Friday", status: "Present" },
        { date: "10-July-2017", day: "Monday", status: "Absent" },
        { date: "11-July-2017", day: "Tuesday", status: "Absent" },
        { date: "12-July-2017", day: "Wednesday", status: "Present" },
        { date: "13-July-2017", day: "Thursday", status: "Absent" },
        { date: "14-July-2017", day: "Friday", status: "Present" },
        { date: "17-July-2017", day: "Monday", status: "Present" },
        { date: "18-July-2017", day: "Tuesday", status: "Present" },
        { date: "19-July-2017", day: "Wednesday", status: "Present" },
        { date: "20-July-2017", day: "Thursday", status: "Present" },
        { date: "21-July-2017", day: "Friday", status: "Present" }
    ];
    $("#teacherattendanceView_table").jqGrid({
        data: mydata,
        datatype: "local",
        height: 450,
        autowidth: true,
       
        rowNum: 20,
        rowList: [10, 20, 30],
        colNames: ['Date', 'Day', 'Status'],
        colModel: [
            { name: 'date', index: 'date', editable: false, sorttype: "int", width: 210, search: true, resizable: false },
            { name: 'day', index: 'day', editable: false, sorttype: "int", width: 420, search: true, resizable: false },
            {
                name: 'status', index: 'status', align:'center', editable: false, resizable: false, width: 420, search: true, edittype: "select",
                editoptions: { value: "PE:Present;AB:Absent;LE:Leave", defaultValue: "PE" }
            }],
        pager: "#pager_list",
        viewrecords: true,
        caption: "Attendance Sheet",
        add: false,
        edit: false,
        cellEdit: false,
        addtext: 'Add',
        edittext: 'Edit',
        hidegrid: false,
        shrinkToFit: false
    });

    // Add selection
    $("#teacherattendanceView_table").setSelection(4, true);


    // Setup buttons
    $("#teacherattendanceView_table").jqGrid('navGrid', '#pager_list',
            { edit: false, add: false, del: false, search: false },
            { height: 300, reloadAfterSubmit: true }
    );

    // Add responsive to jqGrid
    $(window).bind('resize', function () {
        var width = $('.teacherattendanceView_jqGrid').width();

        $('#teacherattendanceView_table').setGridWidth(width, false);
    });

    $("#teacherattendanceView_div").hide();

    $("#teacherattendanceView_table").jqGrid('setGridState', 'hidden');//or 'hidden' 

    $("#teacherAttendanceGrid").click(function (e) {
        if ($("#teacherAttendanceForm").valid()) {

            $("#teacherattendanceView_div").show();
            $("#teacherattendanceView_table").jqGrid('setGridState', 'visible');//or 'hidden' 
            $('#presentField').val('10');
            $('#absentField').val('05');
        }
        return false;
    });

    $('#txtSelectMonth').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'MM yy',
        onClose: function (dateText, inst) {
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
        }
    });

    $("#teacherAttendanceForm").validate({
        errorClass: "my-error-class",
        validClass: "my-valid-class",
        rules: {
            txtEmployeeId: {
                required: true,
                digits: true,
            },
            txtSelectMonth: {
                required: true,
            }
        },
        messages: {
            txtEmployeeId: {
                required: "",
                digits: "Enter number only",
            },
            txtSelectMonth: {
                required: ""
            }
        }
    });

});

