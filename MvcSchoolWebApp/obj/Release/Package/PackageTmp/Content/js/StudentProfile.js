﻿
$(document).ready(function () {
    "use strict";

    $("#personalinfodiv").click(function (e) {
        if ($("#profileInfoForm").valid()) {
            $('#prodiv').removeAttr('hidden', true);
            $('#prodiv2').removeAttr('hidden', true);
            $('#prodiv3').removeAttr('hidden', true);
            $('#prodiv4').removeAttr('hidden', true);
        }
        return false;
    });

   

    
        jQuery.validator.addMethod("lettersonly", function(value, element) {
            return this.optional(element) || /^[a-zA-Z.\s]*$/.test(value);
        }, "Only alphabetical characters"); 
    $("#profileInfoForm").validate({
        errorClass: "my-error-class",
        validClass: "my-valid-class",
        rules: {
            txtSection: {
                required: true,
            },
            txtPersonId: {
                required: true,
                digits: true,
            },
            txtClass: {
                required: true,
            },
            txtCampus: {
                required: true,
            },
        },
        messages: {
            txtSection: {
                required: ""
            },
            txtPersonId: {
                required: "",
                digits: "Enter number only",
            },
            txtClass: {
                required: ""
            },
            txtCampus: {
                required: ""
            },
        }
    });

    $("#formids").validate({
        errorClass: "my-error-class",
        validClass: "my-valid-class",
        rules: {

            //Basic Info
            txtId: {
                required: true,
                digits: true,
            },
            txtSchool: {
                required: true,
                lettersonly: true,
            },
            txtCampus: {
                required: true,
            },
            txtStatus: {
                required: true,
                lettersonly: true,
            },
            txtDepartment: {
                required: true,
                lettersonly: true,
            },
            txtFromDate: {
                required: true,
            },
            txtToDate: {
                required: true,
            },

            //Personal Info
            txtTitle: {
                required: true,
                lettersonly: true,
            },
            txtInitials: {
                required: true,
                lettersonly: true,
            },
            txtFirstName: {
                required: true,
                lettersonly: true,
            },
            txtFatherName: {
                required: true,
                lettersonly: true,
            },
            txtLastName: {
                required: true,
                lettersonly: true,
            },
            txtSecondName: {
                required: true,
                lettersonly: true,
            },
            txtMiddleName: {
                required: true,
                lettersonly: true,
            },

            //Additional Info
            txtIdNumber: {
                required: true,
                digits: true,
            },
            txtBirthDate: {
                required: true,
            },
            txtGender: {
                required: true,
                lettersonly: true,
            },
            txtBirthPlace: {
                required: true,
                lettersonly: true,
            },
            txtNationality: {
                required: true,
                lettersonly: true,
            },
            txtCountryofBirth: {
                required: true,
                lettersonly: true,
            },

            //Address Info
            txtAddressType: {
                required: true,
            },
            txtCO: {
                required: true,
                lettersonly: true,
            },
            txtStreet1: {
                required: true,
            },
            txtStreet2: {
                required: true,
            },
            txtCountry: {
                required: true,
                lettersonly: true,
            },
            txtPhone: {
                required: true,
                digits: true,
            },
            txtZip: {
                required: true,
                digits: true,
            },
            txtCity: {
                required: true,
                lettersonly: true,
            },
            txtDistrict: {
                required: true,
                lettersonly: true,
            },
        },
        messages: {
           
            //Basic Info
            txtId: {
                required: "",
                digits: "Enter number only",
            },
            txtSchool: {
                required: "",
                lettersonly: "Enter letter only",
            },
            txtCampus: {
                required: "",
            },
            txtStatus: {
                required: "",
                lettersonly: "Enter letter only",
            },
            txtDepartment: {
                required: "",
                lettersonly: "Enter letter only",
            },
            txtFromDate: {
                required: "",
            },
            txtToDate: {
                required: "",
            },

            //Personal Info
            txtTitle: {
                required: "",
                lettersonly: "Enter letter only",
            },
            txtInitials: {
                required: "",
                lettersonly: "Enter letter only",
            },
            txtFirstName: {
                required: "",
                lettersonly: "Enter letter only",
            },
            txtFatherName: {
                required: "",
                lettersonly: "Enter letter only",
            },
            txtLastName: {
                required: "",
                lettersonly: "Enter letter only",
            },
            txtSecondName: {
                required: "",
                lettersonly: "Enter letter only",
            },
            txtMiddleName: {
                required: "",
                lettersonly: "Enter letter only",
            },

            //Additional Info
            txtIdNumber: {
                required: "",
                digits: "Enter number only",
            },
            txtBirthDate: {
                required: "",
            },
            txtGender: {
                required: "",
                lettersonly: "Enter letter only",
            },
            txtBirthPlace: {
                required: "",
                lettersonly: "Enter letter only",
            },
            txtNationality: {
                required: "",
                lettersonly: "Enter letter only",
            },
            txtCountryofBirth: {
                required: "",
                lettersonly: "Enter letter only",
            },

            //Address Info
            txtAddressType: {
                required: "",
            },
            txtCO: {
                required: "",
                lettersonly: "Enter letter only",
            },
            txtStreet1: {
                required: "",
            },
            txtStreet2: {
                required: "",
            },
            txtCountry: {
                required: "",
                lettersonly: "Enter letter only",
            },
            txtPhone: {
                required: "",
                digits: "Enter number only",
            },
            txtZip: {
                required: "",
                digits: "Enter number only",
            },
            txtCity: {
                required: "",
                lettersonly: "Enter letter only",
            },
            txtDistrict: {
                required: "",
                lettersonly: "Enter letter only",
            },
        }
    });
    $('#btn-edit').click(function () {

        //Basic Info
        $('#txtId').removeAttr('readonly', true);
        $('#txtSchool').removeAttr('readonly', true);
        $('#txtCampus').removeAttr('disabled', true);
        $('#txtStatus').removeAttr('readonly', true);
        $('#txtDepartment').removeAttr('readonly', true);
        $('#txtFromDate').removeAttr('readonly', true);
        $('#txtToDate').removeAttr('readonly', true);
        $('#uploadiv').removeAttr('hidden', true);
        
        //Perosnal Info
        $('#txtTitle').removeAttr('readonly', true);
        $('#txtInitials').removeAttr('readonly', true);
        $('#txtFirstName').removeAttr('readonly', true);
        $('#txtFatherName').removeAttr('readonly', true);
        $('#txtMiddleName').removeAttr('readonly', true);
        $('#txtSecondName').removeAttr('readonly', true);
        $('#txtLastName').removeAttr('readonly', true);
        
        //Additional Info
        $('#txtIdNumber').removeAttr('readonly', true);
        $('#txtBirthDate').removeAttr('readonly', true);
        $('#txtGender').removeAttr('readonly', true);
        $('#txtBirthPlace').removeAttr('readonly', true);
        $('#txtNationality').removeAttr('readonly', true);
        $('#txtCountryofBirth').removeAttr('readonly', true);
       
        //Address Info
        $('#txtAddressType').removeAttr('disabled', true);
        $('#txtCO').removeAttr('readonly', true);
        $('#txtStreet1').removeAttr('readonly', true);
        $('#txtStreet2').removeAttr('readonly', true);
        $('#txtCountry').removeAttr('readonly', true);
        $('#txtPhone').removeAttr('readonly', true);
        $('#txtZip').removeAttr('readonly', true);
        $('#txtCity').removeAttr('readonly', true);
        $('#txtDistrict').removeAttr('readonly', true);

        $('#divBtnupload').removeAttr('hidden', true);
    });

    $('#btn-update').click(function () {
        if ($("#formids").valid()) {
            //Basic Info
            $('#txtId').prop('readonly', true);
            $('#txtSchool').prop('readonly', true);
            $('#txtCampus').prop('disabled', true);
            $('#txtStatus').prop('readonly', true);
            $('#txtDepartment').prop('readonly', true);
            $('#txtFromDate').prop('readonly', true);
            $('#txtToDate').prop('readonly', true);
            $('#uploadiv').prop('hidden', true);
            

            //Perosnal Info
            $('#txtTitle').prop('readonly', true);
            $('#txtInitials').prop('readonly', true);
            $('#txtFirstName').prop('readonly', true);
            $('#txtFatherName').prop('readonly', true);
            $('#txtMiddleName').prop('readonly', true);
            $('#txtSecondName').prop('readonly', true);
            $('#txtLastName').prop('readonly', true);

            //Additional Info
            $('#txtIdNumber').prop('readonly', true);
            $('#txtBirthDate').prop('readonly', true);
            $('#txtGender').prop('readonly', true);
            $('#txtBirthPlace').prop('readonly', true);
            $('#txtNationality').prop('readonly', true);
            $('#txtCountryofBirth').prop('readonly', true);

            //Address Info
            $('#txtAddressType').prop('disabled', true);
            $('#txtCO').prop('readonly', true);
            $('#txtStreet1').prop('readonly', true);
            $('#txtStreet2').prop('readonly', true);
            $('#txtCountry').prop('readonly', true);
            $('#txtPhone').prop('readonly', true);
            $('#txtZip').prop('readonly', true);
            $('#txtCity').prop('readonly', true);
            $('#txtDistrict').prop('readonly', true);

           
            $('#divBtnupload').prop('hidden', true);
        }
    });
    
    $('#btn-image').click(function () {
            $('#imageuploader').modal('show');
        });
});