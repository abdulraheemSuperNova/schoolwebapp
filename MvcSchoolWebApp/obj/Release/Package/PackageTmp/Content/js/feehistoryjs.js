﻿

$(document).ready(function () {


    // Examle data for jqGrid
    var mydata = [
        { Sid: "1001", stdname: "Ahmed Ali", Date: "10may2017", Feeamount: "1000/" },
        { Sid: "1002", stdname: "Shayan", Date: "10may2017", Feeamount: "1000/" },
        { Sid: "1003", stdname: "Bilal khan", Date: "10may2017", Feeamount: "1000/" },
        { Sid: "1004", stdname: "Waleed abbasi", Date: "10may2017", Feeamount: "1000/" },
        { Sid: "1005", stdname: "Moiz", Date: "10may2017", Feeamount: "1000/" },

    ];

    // Configuration for jqGrid Example 1


    // Configuration for jqGrid Example 2
    $("#feehistorygrid").jqGrid({
        data: mydata,
        datatype: "local",
        height: 450,
        autowidth: true,
        rowNum: 20,
        rowList: [10, 20, 30],
        colNames: ['Student Id', 'Name', 'Date', 'Fee Amount'],
        colModel: [
            //width: "350", resizable: false, editable: false, align: 'center'
            { name: 'Sid', index: 'id', editable: false, width: 100, align: 'left', resizable: false, sorttype: "int", search: true },
            { name: 'stdname', index: 'quizname', editable: false, width: 600, align: 'left', resizable: false, search: true },

            { name: 'Date', index: 'Date', editable: false, width: 165, align: 'left', resizable: false, search: true },

            { name: 'Feeamount', index: 'status', editable: false, width: 100, align: 'left', resizable: false, sortable: false },


        ],

        //gridComplete: function () {
        //    var ids = jQuery("#feehistorygrid").getDataIDs();
        //    for (var i = 0; i < ids.length; i++) {
        //        var cl = ids[i];
                
        //        view = "<input style='height:18px;width:75px;' type='button' value='View' class='btn btn-xs btn-primary'>";
        //        download = "&nbsp;<input style='height:18px;width:75px;' type='button' class='btn btn-xs btn-primary' value='Download' />";

        //        jQuery("#feehistorygrid").setRowData(ids[i], { status: view + download })
        //    }
        //},

        pager: "#feehistory_pager",
        viewrecords: true,
        //caption: "Quiz",
        add: true,
        edit: true,
        addtext: 'Add',
        edittext: 'Edit',
        hidegrid: false,
        shrinkToFit: false,
        caption: "Fee History"
    });

    // Add selection
    $("#feehistorygrid").setSelection(4, true);


    // Setup buttons
    $("#feehistorygrid").jqGrid('navGrid', '#feehistory_pager',
            { edit: true, add: true, del: true, search: true },
            { height: 200, reloadAfterSubmit: true }
    );

    // Add responsive to jqGrid
    $(window).bind('resize', function () {
        var width = $('.feehistory_wrapper').width();

        $('#feehistorygrid').setGridWidth(width, false);
    });

    $("#feehistorydiv").hide();

    $("#feehistorygrid").jqGrid('setGridState', 'hidden');//or 'hidden' 

    $("#btnfeehistory").click(function (e) {
        if ($("#feehistoryform").valid()) {

            $("#feehistorydiv").show();
            $("#feehistorygrid").jqGrid('setGridState', 'visible');//or 'hidden' 
        }
        return false;
    });

    $("#feehistoryform").validate({
        errorClass: "my-error-class",
        validClass: "my-valid-class",
        rules: {
            txtSubject: {
                required: true,
            },
            txtStudentId: {
                required: true,
            },
            txtSelectMonth: {
                required: true,
            },
            txtClass: {
                required: true,
            },
            txtCampus: {
                required: true,
            },
            txtDate: {
                required: true,
            }
        },
        messages: {
            txtSubject: {
                required: "",
            },
            txtStudentId: {
                required: ""
            },
            txtSelectMonth: {
                required: ""
            },
            txtClass: {
                required: ""
            },
            txtCampus: {
                required: ""
            },
            txtDate: {
                required: ""
            }
        }
    });

});


//$(document).ready(function()
//{
//    $("#btnviewresult").click(function () {

//        $("#resultgrid").show();

//        //$("#p1").hide();
//        //$("#div1").text($("#p1").css("display"));
//    });
//});

