﻿$(document).ready(function () {
    $("#btn-submit").click(function (e) {
        if ($("#uploadform").valid()) {
            
        }
        return false;
    });
    $("#uploadform").validate({
        errorClass: "my-error-class",
        validClass: "my-valid-class",
        rules: {
            txtSubject: {
                required: true,
            },
            txtSection: {
                required: true,
            },
            txtCategories: {
                required: true,
            },
            txtClass: {
                required: true,
            },
            txtCampus: {
                required: true,
            },
            txtDate: {
                required: true,
            }
        },
        messages: {
            txtSubject: {
                required: "",
            },
            txtSection: {
                required: ""
            },
            txtCategories: {
                required: "",
            },
            txtClass: {
                required: ""
            },
            txtCampus: {
                required: ""
            },
            txtDate: {
                required: ""
            }
        }
    });

});