﻿

$(document).ready(function () {


    // Examle data for jqGrid
    var mydata = [
        { id: "1", quizname: "Quiz1", status: "view / download" },
        { id: "2", quizname: "Quiz2", status: "view / download" },
        { id: "3", quizname: "Quiz3", status: "view / download" },
        { id: "4", quizname: "Quiz4", status: "view / download" },
        { id: "5", quizname: "Quiz5", status: "view / download" },
        { id: "6", quizname: "Quiz6", status: "view / download" },

    ];

    // Configuration for jqGrid Example 1


    // Configuration for jqGrid Example 2
    $("#table_list_2").jqGrid({
        data: mydata,
        datatype: "local",
        height: 450,
        autowidth: true,
        rowNum: 20,
        rowList: [10, 20, 30],
        colNames: ['S No', 'Quiz Name', 'Action'],
        colModel: [
            //width: "350", resizable: false, editable: false, align: 'center'
            { name: 'id', index: 'id', editable: false, width: 100, align: 'left', resizable: false, sorttype: "int", search: true },
            { name: 'quizname', index: 'quizname', editable: false, width: 780, align: 'left', resizable: false, search: true },
            //{ name: 'status', index: 'status', editable: false, width: 50, align: 'left', resizable: false}

            { name: 'status', index: 'status', editable: false, width: 180, align: 'left', resizable: false, sortable: false },


        ],

        gridComplete: function () {
            var ids = jQuery("#table_list_2").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var cl = ids[i];
                //class='btn btn-xs btn-info'
                view = "<input style='height:18px;width:75px;' type='button' onclick='getOnClick()' value='View' class='btn btn-xs btn-primary'>";
                download = "&nbsp;<input style='height:18px;width:75px;' type='button' class='btn btn-xs btn-primary' value='Download' />";

                jQuery("#table_list_2").setRowData(ids[i], { status: view + download })
            }
        },

        pager: "#pager_list_2",
        viewrecords: true,
        //caption: "Quiz",
        add: true,
        edit: true,
        addtext: 'Add',
        edittext: 'Edit',
        hidegrid: false,
        shrinkToFit: false,
        caption: "Student Performance"
    });

    // Add selection
    $("#table_list_2").setSelection(4, true);


    // Setup buttons
    $("#table_list_2").jqGrid('navGrid', '#pager_list_2',
            { edit: true, add: true, del: true, search: true },
            { height: 200, reloadAfterSubmit: true }
    );

    // Add responsive to jqGrid
    $(window).bind('resize', function () {
        var width = $('.jqGrid_wrapper').width();

        $('#table_list_2').setGridWidth(width, false);
    });

    $("#quiz_div").hide();

    $("#table_list_2").jqGrid('setGridState', 'hidden');//or 'hidden' 

    $("#btnviewresult").click(function (e) {
        if ($("#resultform").valid()) {
            $("#quiz_div").show();
            $("#table_list_2").jqGrid('setGridState', 'visible');//or 'hidden' 
        }
        return false;
    });

    $("#resultform").validate({
        errorClass: "my-error-class",


        rules: {
            txtSubject: {
                required: true,
            },
            txtSection: {
                required: true,
            },
            txtName: {
                required: true,

            },
            txtClass: {
                required: true,
            },
            txtCampus: {
                required: true,
            }
        },
        messages: {
            txtSubject: {
                required: "",
            },
            txtSection: {
                required: ""
            },
            txtName: {
                required: ""
            },
            txtClass: {
                required: ""
            },
            txtCampus: {
                required: ""
            }
        }

    });


});
function getOnClick() {

    //$.ajax({
    //    type: 'POST',
    //    url: "/result/getreport",
    //    success: function (result) {
    //    },
    //    error: function () {
    //        alert("error in alert");
    //    }
    //});
    //  window.location.href = "/result/getreport"; 
    window.location.href = "/result/getreport";
}



