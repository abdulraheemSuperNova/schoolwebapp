﻿

$(document).ready(function () {


    // Examle data for jqGrid
    var mydata = [
        { id: "1", filename: "file1.text", Date: "10may2017", status: "view / download" },
        { id: "2", filename: "file2.text", Date: "20may2017", status: "view / download" },
        { id: "3", filename: "file3.text", Date: "25may2017", status: "view / download" },
        { id: "4", filename: "file4.text", Date: "8jun2017", status: "view / download" },
        { id: "5", filename: "file5.text", Date: "17jun2017", status: "view / download" },
        { id: "6", filename: "file6.text", Date: "3jul2017", status: "view / download" },


    ];

    // Configuration for jqGrid Example 1


    // Configuration for jqGrid Example 2
    $("#classtimetable_grid").jqGrid({
        data: mydata,
        datatype: "local",
        height: 450,
        autowidth: true,
        rowNum: 20,
        rowList: [10, 20, 30],
        colNames: ['S No', 'Name', 'Date', 'Action'],
        colModel: [
            //width: "350", resizable: false, editable: false, align: 'center'
           { name: 'id', index: 'id', editable: false, width: 100, align: 'left', resizable: false, sorttype: "int", search: true },
            { name: 'filename', index: 'quizname', editable: false, width: 600, align: 'left', resizable: false, search: true },

            { name: 'Date', index: 'Date', editable: false, width: 165, align: 'left', resizable: false, search: true },

            { name: 'status', index: 'status', editable: false, width: 180, align: 'left', resizable: false, sortable: false },


        ],

        gridComplete: function () {
            var ids = jQuery("#classtimetable_grid").getDataIDs();
            for (var i = 0; i < ids.length; i++) {
                var cl = ids[i];
                //class='btn btn-xs btn-info'
                view = "<input style='height:18px;width:75px;' type='button' value='View' class='btn btn-xs btn-primary'>";
                download = "&nbsp;<input style='height:18px;width:75px;' type='button' class='btn btn-xs btn-primary' value='Download' />";

                jQuery("#classtimetable_grid").setRowData(ids[i], { status: view + download })
            }
        },

        pager: "#classtimetable_pager",
        viewrecords: true,
        //caption: "Quiz",
        add: true,
        edit: true,
        addtext: 'Add',
        edittext: 'Edit',
        hidegrid: false,
        shrinkToFit: false,
        caption: "Class Timetable"
    });

    // Add selection
    $("#classtimetable_grid").setSelection(4, true);


    // Setup buttons
    $("#classtimetable_grid").jqGrid('navGrid', '#classtimetable_pager',
            { edit: true, add: true, del: true, search: true },
            { height: 200, reloadAfterSubmit: true }
    );

    // Add responsive to jqGrid
    $(window).bind('resize', function () {
        var width = $('.classtimetable_wrapper').width();

        $('#classtimetable_grid').setGridWidth(width, false);
    });

    $("#classtimetable_div").hide();

    $("#classtimetable_grid").jqGrid('setGridState', 'hidden');//or 'hidden' 

    $("#btnclasstimetable").click(function (e) {
        if ($("#classtimetableform").valid()) {

            $("#classtimetable_div").show();
            $("#classtimetable_grid").jqGrid('setGridState', 'visible');//or 'hidden' 
        }
        return false;
    });

    $("#classtimetableform").validate({
        errorClass: "my-error-class",
        validClass: "my-valid-class",
        rules: {
            txtSubject: {
                required: true,
            },
            txtSection: {
                required: true,
            },
            txtTeacherName: {
                required: true,
            },
            txtClass: {
                required: true,
            },
            txtCampus: {
                required: true,
            },
            txtDate: {
                required: true,
            }
        },
        messages: {
            txtSubject: {
                required: "",
            },
            txtSection: {
                required: ""
            },
            txtTeacherName: {
                required: ""
            },
            txtClass: {
                required: ""
            },
            txtCampus: {
                required: ""
            },
            txtDate: {
                required: ""
            }
        }
    });
});


//$(document).ready(function()
//{
//    $("#btnviewresult").click(function () {

//        $("#resultgrid").show();

//        //$("#p1").hide();
//        //$("#div1").text($("#p1").css("display"));
//    });
//});

