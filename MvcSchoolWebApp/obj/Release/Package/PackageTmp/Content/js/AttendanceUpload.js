﻿
$(document).ready(function () {
    "use strict";
    // Examle data for jqGrid
    var mydata = [
        { id: "22001", studentname: "Abdul Basit", status: "Present" },
        { id: "22002", studentname: "Ahmed Zamir", status: "Absent" },
        { id: "22003", studentname: "Ali Manzar", status: "Absent" },
        { id: "22004", studentname: "Amir udin Ayaz", status: "Present" },
        { id: "22005", studentname: "Azzaz ul Haq", status: "Present" },
        { id: "22006", studentname: "Danish Walani", status: "Absent" },
        { id: "22007", studentname: "Hamza Iqbal", status: "Absent" },
        { id: "22008", studentname: "Mahboob Alam", status: "Present" },
        { id: "22009", studentname: "Moiz Iqbal", status: "Absent" },
        { id: "22010", studentname: "Mustafa Quresh", status: "Present" },
        { id: "22011", studentname: "Salman Khan", status: "Present" },
        { id: "22012", studentname: "Shaheer Ali Agha", status: "Present" },
        { id: "22013", studentname: "Syed Saad Ali", status: "Present" },
        { id: "22014", studentname: "Talha Hussain Chohan", status: "Present" },
        { id: "22015", studentname: "Taha Nadeem", status: "Present" }
    ];
    $("#attendance_table").jqGrid({
        styleUI: 'Bootstrap',
        height: 450,
        autowidth: true,
        //shrinkToFit: true,
        rowNum: 20,
        colNames: ['Id No', 'Student Name', 'Status'],
        colModel: [
            { name: 'id', index: 'id', editable: true, sorttype: "int", width: 180, search: true, resizable: false },
            { name: 'studentname', index: 'studentname', editable: true, width: 700, resizable: false },
            {
                name: 'status', index: 'status', editable: true,align: 'center', resizable: false, width: 190, search: true, edittype: "select",
                editoptions: { value: "PE:Present;AB:Absent;LE:Leave", defaultValue: "PE" }
            }],
        data: mydata,
        datatype: "local",
        iconSet: "fontAwesome",
        idPrefix: "g5_",
        sortname: "invdate",
        sortorder: "desc",
        threeStateSort: true,
        sortIconsBeforeText: true,
        headertitles: true,
        cellEdit: true,
        cellsubmit: 'clientArray',
        editable: true,
        pager: "#pager_list",
        viewrecords: true,
        caption: "Attendance Sheet",
        add: true,
        edit: true,
        addtext: 'Add',
        edittext: 'Edit',
        hidegrid: false,
        shrinkToFit: false
    });

    // Add selection
    //$("#attendance_table").setSelection(4, true);


    // Setup buttons
    $("#attendance_table").jqGrid('navGrid', '#pager_list',
            { edit: true, add: true, del: true, search: true },
            { height: 300, reloadAfterSubmit: true }
    );

    // Add responsive to jqGrid
    $(window).bind('resize', function () {
        var width = $('.attendance_jqGrid').width();
        $('#attendance_table').setGridWidth(width,false);
    });

    $("#attendance_div").hide();

    $("#attendance_table").jqGrid('setGridState', 'hidden');//or 'hidden' 

    $("#showGrid").click(function (e) {
        if ($("#attendanceForm").valid()) {
            $("#attendance_div").show();
            $("#attendance_table").jqGrid('setGridState', 'visible');//or 'hidden' 
        }
       
        return false;
    });

    jQuery.validator.addMethod("lettersonly", function (value, element) {
        return this.optional(element) || /^[a-zA-Z.\s]*$/.test(value);
    }, "Only alphabetical characters");

    $("#datepickerAttendance").datepicker();

    $("#attendanceForm").validate({
        errorClass: "my-error-class",
        validClass: "my-valid-class",
        rules: {
            txtSubject: {
                required: true,
            },
            txtSection: {
                required: true,
            },
            txtTeacherName: {
                required: true,
                lettersonly: true,
            },
            txtClass: {
                required: true,
            },
            txtCampus: {
                required: true,
            },
            txtDate: {
                required: true,
            }
        },
        messages: {
            txtSubject: {
                required: "",
            },
            txtSection: {
                required: ""
            },
            txtTeacherName: {
                required: "",
                lettersonly: "Enter letters only",
            },
            txtClass: { 
                required: ""
            },
            txtCampus: {
                required: ""
            },
            txtDate: {
                required: ""
            }
        }        
    });

});

