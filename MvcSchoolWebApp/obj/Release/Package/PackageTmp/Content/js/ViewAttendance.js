﻿$(document).ready(function () {
    "use strict";
    // Examle data for jqGrid
    var mydata = [
        { day: "01", status: "Present" },
        { day: "02", status: "Absent" },
        { day: "03", status: "Absent" },
        { day: "04", status: "Present" },
        { day: "05", status: "Present" },
        { day: "06", status: "Absent" },
        { day: "07", status: "Absent" },
        { day: "08", status: "Present" },
        { day: "09", status: "Absent" },
        { day: "10", status: "Present" },
        { day: "11", status: "Present" },
        { day: "12", status: "Present" },
        { day: "13", status: "Present" },
        { day: "14", status: "Present" },
        { day: "15", status: "Present" }
    ];
    $("#attendanceView_table").jqGrid({
        data: mydata,
        datatype: "local",
        height: 450,
        autowidth: true,
        
        rowNum: 20,
        rowList: [10, 20, 30],
        colNames: ['Day', 'Status'],
        colModel: [
            { name: 'day', index: 'day', editable: false, sorttype: "int", width: 520, search: true, resizable: false },
            {
                name: 'status', index: 'status', editable: false, resizable: false, width: 540, search: true, edittype: "select",
                editoptions: { value: "PE:Present;AB:Absent;LE:Leave", defaultValue: "PE" }
            }],
        pager: "#pager_list",
        viewrecords: true,
        caption: "Attendance Sheet",
        add: false,
        edit: false,
        cellEdit: false,
        addtext: 'Add',
        edittext: 'Edit',
        hidegrid: false,
        shrinkToFit: false
    });

    // Add selection
    $("#attendanceView_table").setSelection(4, true);


    // Setup buttons
    $("#attendanceView_table").jqGrid('navGrid', '#pager_list',
            { edit: false, add: false, del: false, search: false },
            { height: 300, reloadAfterSubmit: true }
    );

    // Add responsive to jqGrid
    $(window).bind('resize', function () {
        var width = $('.attendanceView_jqGrid').width();

        $('#attendanceView_table').setGridWidth(width,false);
    });

    $("#attendanceView_div").hide();

    $("#attendanceView_table").jqGrid('setGridState', 'hidden');//or 'hidden' 

    $("#veiwAttendance").click(function (e) {
        if ($("#viewAttendanceForm").valid()) {

            $("#attendanceView_div").show();
            $("#attendanceView_table").jqGrid('setGridState', 'visible');//or 'hidden' 
        }
        return false;
    });

    $('#ViewAttendaceDate').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        dateFormat: 'MM yy',
        onClose: function (dateText, inst) {
            var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
            var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
            $(this).datepicker('setDate', new Date(year, month, 1));
        }
    });

    $("#viewAttendanceForm").validate({
        errorClass: "my-error-class",
        validClass: "my-valid-class",
        rules: {
            txtStudentId: {
                required: true,
                digits: true,
            },
            txtPickaMonth: {
                required: true,
            },
            txtSubject: {
                required: true,
            }
        },
        messages: {
            txtStudentId: {
                required: "",
                digits: "Enter number only"
            },
            txtPickaMonth: {
                required: ""
            },
            txtSubject: {
                required: ""
            }
        }
    });

});

