﻿
$(document).ready(function () {

    // Examle data for jqGrid
    var mydata = [
            { id: "22001", name: "Abdul Basit", marks: "74", percentage: "70%" },
            { id: "22002", name: "Ahmed Zamir", marks: "64", percentage: "60%" },
            { id: "22003", name: "Ali Manzar", marks: "54", percentage: "70%" },
            { id: "22004", name: "Amir udin Ayaz", marks: "65", percentage: "65%" },
            { id: "22005", name: "Azzaz ul Haq", marks: "77", percentage: "77%" },
            { id: "22006", name: "Danish Walani", marks: "69", percentage: "69%" },
            { id: "22007", name: "Hamza Iqbal", marks: "68", percentage: "68%" },
            { id: "22008", name: "Mahboob Alam", marks: "72", percentage: "72%" },
            { id: "22009", name: "Moiz Iqbal", marks: "70", percentage: "70%" },
            { id: "22010", name: "Mustafa Quresh", marks: "84", percentage: "84%" },
            { id: "22011", name: "Salman Khan", marks: "85", percentage: "85%" },
            { id: "22012", name: "Shaheer Ali Agha", marks: "63", percentage: "63%" },
            { id: "22013", name: "Syed Saad Ali", marks: "67", percentage: "67%" },
            { id: "22014", name: "Talha Hussain Chohan", marks: "72", percentage: "72%" },
            { id: "22015", name: "Taha Nadeem", marks: "83", percentage: "83%" }
    ];

    $("#marks_table").jqGrid({
        data: mydata,
        datatype: "local",
        height: 450,
        autowidth: true,
       
        rowNum: 20,
        rowList: [10, 20, 30],
        colModel: [
             { name: "id", label: "Id", width: 120 },
            { name: "name", label: "Student Name", width: 730  },
            { name: "marks", label: "Marks", editable: true, width: 100 },
            { name: "percentage", label: "Percentage", width: 100}
        ],
        pager: "#pager_list",
        viewrecords: true,
        caption: "Marks Sheet",
        add: true,
        edit: true,
        cellEdit: true,
        sortIconsBeforeText: true,
        addtext: 'Add',
        edittext: 'Edit',
        hidegrid: false,
        shrinkToFit: false
    });

    // Add selection
 //   $("#marks_table").setSelection(4, true);


    // Setup buttons
    $("#marks_table").jqGrid('navGrid', '#pager_list',
            { edit: true, add: true, del: true, search: true },
            { height: 300, reloadAfterSubmit: true }
    );

    // Add responsive to jqGrid
    $(window).bind('resize', function () {
        var width = $('.marks_jqGrid').width();

        $('#marks_table').setGridWidth(width, false);
    });

    $("#marks_div").hide();

    $("#marks_table").jqGrid('setGridState', 'hidden');//or 'hidden'

    $("#showGrid1").click(function (e) {
        if ($("#marksuploadform").valid()){
            $("#marks_div").show();
            $("#marks_table").jqGrid('setGridState', 'visible');//or 'hidden' 
        }
        return false;
    });
    
    $('#datepickerMarks').datepicker();

    $("#datepickerAttendance").datepicker();

    jQuery.validator.addMethod("lettersonly", function (value, element) {
        return this.optional(element) || /^[a-zA-Z.\s]*$/.test(value);
    }, "Only alphabetical characters");

    $("#marksuploadform").validate({
        errorClass: "my-error-class",
        validClass: "my-valid-class",
        rules: {
            txtSubject: {
                required: true,
            },
            txtSection: {
                required: true,
            },
            txtTeacherName: {
                required: true,
                lettersonly: true
            },
            txtClass: {
                required: true,
            },
            txtCampus: {
                required: true,
            },
            txtDate: {
                required: true,
            },
            txtMarks: {
                required: true,
                number: true,
            },
            txtModule: {
                required: true,
            }
        },
        messages: {
            txtSubject: {
                required: "",
            },
            txtSection: {
                required: ""
            },
            txtTeacherName: {
                required: "",
                lettersonly: "Enter letters only"
            },
            txtClass: {
                required: ""
            },
            txtCampus: {
                required: ""
            },
            txtDate: {
                required: ""
            },
            txtMarks: {
                required: "",
                number: "Enter numbers only",
            },
            txtModule: {
                required: "",
            }
        }
    });
});

