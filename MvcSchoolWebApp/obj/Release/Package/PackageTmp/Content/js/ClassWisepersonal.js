﻿
$(document).ready(function () {
    "use strict";
    // Examle data for jqGrid
    var mydata = [
        { id: "22001", studentname: "Abdul Basit", fathername: "Basit" },
        { id: "22002", studentname: "Ahmed Zamir", fathername: "Zamir" },
        { id: "22003", studentname: "Ali Manzar", fathername: "Manzar" },
        { id: "22004", studentname: "Amir udin Ayaz", fathername: "Ayaz" },
        { id: "22005", studentname: "Azzaz ul Haq", fathername: " Azaz" },
        { id: "22006", studentname: "Danish Walani", fathername: "Walani" },
        { id: "22007", studentname: "Hamza Iqbal", fathername: "Iqbal" },
        { id: "22008", studentname: "Mahboob Alam", fathername: "Alam" },
        { id: "22009", studentname: "Moiz Iqbal", fathername: "Iqbal" },
        { id: "22010", studentname: "Mustafa Quresh", fathername: "Quresh" },
        { id: "22011", studentname: "Salman Khan", fathername: "Khan" },
        { id: "22012", studentname: "Shaheer Ali Agha", fathername: "Ali Agha" },
        { id: "22013", studentname: "Syed Saad Ali", fathername: "Ali" },
        { id: "22014", studentname: "Talha Hussain Chohan", fathername: "Chohan" },
        { id: "22015", studentname: "Taha Nadeem", fathername: "Nadeem" }
    ];
    $("#classwise_table").jqGrid({
        styleUI: 'Bootstrap',
        height: 450,
        autowidth: true,
        //shrinkToFit: true,
        rowNum: 20,
        colNames: ['Id No', 'Student Name', 'Father Name'],
        colModel: [
            { name: 'id', index: 'id', editable: true, sorttype: "int", width: 180, search: true, resizable: false },
            { name: 'studentname', index: 'studentname', editable: true, width: 700, resizable: false },
            { name: 'fathername', index: 'fathername', editable: true, width: 700, resizable: false },
            ],
        data: mydata,
        datatype: "local",
        iconSet: "fontAwesome",
        idPrefix: "g5_",
        sortname: "invdate",
        sortorder: "desc",
        threeStateSort: true,
        sortIconsBeforeText: true,
        headertitles: true,
        cellEdit: true,
        cellsubmit: 'clientArray',
        editable: true,
        pager: "#pager_list",
        viewrecords: true,
        caption: "Profile Sheet",
        add: true,
        edit: true,
        addtext: 'Add',
        edittext: 'Edit',
        hidegrid: false,
        shrinkToFit: false
    });

    // Setup buttons
    $("#classwise_table").jqGrid('navGrid', '#pager_list',
            { edit: true, add: true, del: true, search: true },
            { height: 300, reloadAfterSubmit: true }
    );

    // Add responsive to jqGrid
    $(window).bind('resize', function () {
        var width = $('.classwise_jqGrid').width();
        $('#classwise_table').setGridWidth(width, false);
    });

    $("#classwise_div").hide();

    $("#classwise_table").jqGrid('setGridState', 'hidden');//or 'hidden' 

    $("#showdiv").click(function (e) {
        if ($("#classWiseForm").valid()) {

            $("#classwise_div").show();
            $("#classwise_table").jqGrid('setGridState', 'visible');//or 'hidden'
        }
        return false;
    });

    $("#classWiseForm").validate({
        errorClass: "my-error-class",
        validClass: "my-valid-class",
        rules: {
            txtSection: {
                required: true,
            },
            txtPersonId: {
                required: true,
            },
            txtClass: {
                required: true,
            },
            txtCampus: {
                required: true,
            }
        },
        messages: {
            txtSection: {
                required: ""
            },
            txtPersonId: {
                required: ""
            },
            txtClass: {
                required: ""
            },
            txtCampus: {
                required: ""
            }
        }
    });

});
