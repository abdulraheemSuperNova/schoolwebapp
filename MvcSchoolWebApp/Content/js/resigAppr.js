﻿


$(document).ready(function () {
    loadResgAppdata();
    var id = document.getElementById("txtenddateRAPP");
    if (id != null) {
        $('#txtenddateRAPP').datepicker({
            format: 'dd-MM-yyyy',
            autoclose: true,
            startDate: date
        });

    }
});



function loadResgAppdata() {
    var ctx = document.getElementById("resigapp_table");
    if (ctx != null) {
        var $screensize = $("#resigapp_div").width() - 42;
        var $size = ($screensize / 100);
        $("#resigapp_table").jqGrid({
            url: '/MSS/GetResgAppr',
            postData: { reqtyp: "10" },
            datatype: 'json',
            mtype: 'POST',
            autowidth: true,
            height: 270,

            serializeGridData: function (postData) {
                return JSON.stringify(postData);
            },

            ajaxGridOptions: { contentType: "application/json" },
            loadonce: true,

            colNames: ['Action', 'Employee Id', 'Employee Name', 'Date', 'Status', 'Overall Status'],
            colModel: [
                { name: 'act', index: 'act', width: $size * 20, sortable: false, resizable: false },
                { name: 'empid', index: 'empid', resizable: false, width: $size * 15, editable: false, editoptions: { readonly: true, size: 10 }, sortable: true },
                { name: 'empname', index: 'empname', resizable: false, width: $size * 20, editable: false, size: 100, sortable: true, align: 'bottam', },
                { name: 'dates', index: 'dates', resizable: false, width: $size * 20, editable: false, size: 100, sortable: true },
                { name: 'stat', index: 'stat', resizable: false, width: $size * 20, editable: false, size: 100, sortable: true },
                { name: 'overalstat', index: 'overalstat', resizable: false, width: $size * 20, editable: false, size: 100, sortable: true }
            ],

            gridComplete: function () {
                var ids = jQuery("#resigapp_table").getDataIDs();
                for (var i = 0; i < ids.length; i++) {
                    var cl = ids[i];
                    recomendresig = "<input style='height:18px;' type='button' value='Acceptance' onclick= 'getSelectedRow_recodresg();' class='btn btn-xs btn-danger'>";
                    apprresig = "<input style='height:18px;margin-left: 5px;' type='button' value='Approval' onclick= 'getSelectedRow_appresg();' class='btn btn-xs btn-danger'>";
                    jQuery("#resigapp_table").setRowData(ids[i], { act: recomendresig + apprresig })
                }
            },

            pager: '#resigapp_pager',
            rowNum: 10,
            rowList: [10, 20, 30],
            viewrecords: true,
            gridview: true,
            jsonReader: {
                page: function (obj) { return 1; },
                total: function (obj) { return 1; },
                root: function (obj) { return obj.d; },
                repeatitems: false,
                id: "0"
            },
            shrinkToFit: false

        });

        // Setup buttons
        $("#resigapp_table").jqGrid('navGrid', '#resigapp_pager',
            { edit: true, add: true, del: true, search: true },
            { height: 300, reloadAfterSubmit: true }
        );

        // Add responsive to jqGrid
        $(window).bind('resize', function () {
            var width = $('.resigapp_jqGrid').width();
            $('#resigapp_table').setGridWidth(width, false);
        });
    }
}


function getSelectedRow_recodresg() {
    var selRowId = $('#resigapp_table').jqGrid('getGridParam', 'selrow');
    var empId = $('#resigapp_table').jqGrid('getCell', selRowId, 'empid');
    var stat = $('#resigapp_table').jqGrid('getCell', selRowId, 'stat');
    if (stat == "Pending") {
        if (empId == undefined) {
            show_err_alert_js('Please Select a row First');
        }
        else {

            $.ajax({
                type: "POST",
                url: encodeURI("../MSS/getresgDetails"),
                data: { empId: empId },
                success: function (data) {
                    fillRecResgpopup(data);

                },
                error: function (data) {
                    show_err_alert_js('Found some error! Please try again');
                }
            });
        }
    }
}

function getSelectedRow_appresg() {
    var selRowId = $('#resigapp_table').jqGrid('getGridParam', 'selrow');
    var empId = $('#resigapp_table').jqGrid('getCell', selRowId, 'empid');
    var loanstat = $('#resigapp_table').jqGrid('getCell', selRowId, 'stat');
    if (loanstat == "Processing") {
        if (empId == undefined) {
            show_err_alert_js('Please Select a row First');
        }
        else {
            $.ajax({
                type: "POST",
                url: encodeURI("../MSS/getresgDetails"),
                data: { empId: empId },
                success: function (data) {
                    
                    //$('#RAPPAppPopup').modal('show');
                    fillresgpopup(data);

                },
                error: function (data) {
                    show_err_alert_js('Found some error! Please try again');
                }
            });
        }
    }
}

function fillRecResgpopup(data) {
    if (data.length != 0) {
        $('#txtrecEmpidRAP').val(data[0].empid);
        $('#txtrecreqnoRAPP').val(data[0].recordno);
        $('#txtrecEmpnameRAPP').val(data[0].empname);
        $('#txtrecjoinRAPP').val(data[0].joindate);
        $('#txtrecdesigRAPP').val(data[0].design);
        $('#txtrecresgapbdate').val(data[0].begdate);
        var items = [];
        items.push("<option value=" + data[0].loantyp[0].Value + ">" + data[0].loantyp[0].Text + "</option>");
        $("#txtResigntypRAP").html(items.join(' '));
        $('#ResigRecodPopup').modal('show');
    }
    else
        show_err_alert_js('Request already done!');
}
function fillresgpopup(data) {
   
    if (data.length != 0) {
        $('#txtresigEmpid').val(data[0].empid);
        $('#txtresigresigreqno').val(data[0].recordno);
        $('#txtresigEmpname').val(data[0].empname);
        $('#txtresigresigjoin').val(data[0].joindate);
        $('#txtresigresigdesig').val(data[0].design);
        $('#txtrecresgappbdate').val(data[0].begdate);
        var items = [];
        items.push("<option value=" + data[0].loantyp[0].Value + ">" + data[0].loantyp[0].Text + "</option>");
        $("#txtResigntypRAPP").html(items.join(' '));
        $('#RAPPAppPopup2').modal('show');
    }
    else
        show_err_alert_js('Request already done!');
}

//Recommend Resig Reject
$("#btnrecodRecrejRAP").click(function () {

    $.ajax({
        type: "POST",
        url: encodeURI("../MSS/rejectRecomendresg"),
        data: {
            empid: $('#txtrecEmpidRAP').val(),
            reqno: $('#txtrecreqnoRAPP').val(),
            subpagtype: $("#txtResigntypRAP > option:selected").attr("value"),
            comment: $('#txtRecodComments').val(),
        },
        success: function (data) {
            if (data == "Success") {
                $('#ResigRecodPopup').modal('hide');
                $('#resigapp_table').trigger('reloadGrid');
                show_suc_alert_js('Successfully resignation rejected!');
                window.location.href = '/MSS/resignApproval';
            }
        },
        error: function (data) {
            show_err_alert_js('Found some error! Please try again');
        }
    });

});


//Recommended Resig Accepted
$("#btnresgcsubmitRAPP").click(function () {
   
    $.ajax({
        type: "POST",
        url: encodeURI("../MSS/acceptResgByRecomder"),
        data: {
            empid: $('#txtrecEmpidRAP').val(),
            reqno: $('#txtrecreqnoRAPP').val(),
            subpagtype: $("#txtResigntypRAP > option:selected").attr("value"),
            startdate: $('#txtloanloanappbdate').val(),
            enddate: $('#txtloanloanappedate').val(),
            apprrecodtypid: $("#txtApprRecomend > option:selected").attr("value"),
            comment: $('#txtloanRecodComments').val(),
            reqtype: "10"
        },
        success: function (data) {
            if (data == "Success") {
                $('#RAPPAppPopup').modal('hide');
                $('#resigapp_table').trigger('reloadGrid');
                show_suc_alert_js('Successfully resignation accepted!');
                window.location.href = '/MSS/resignApproval';
            }

        },
        error: function (data) {
            show_err_alert_js('Found some error! Please try again');
        }
    });
});

//Approver Resig Reject
$("#btnresgRecrejRAPP").click(function () {

    $.ajax({
        type: "POST",
        url: encodeURI("../MSS/rejectApprresg"),
        data: {
            empid: $('#txtresigEmpid').val(),
            reqno: $('#txtresigresigreqno').val(),
            subpagtype: $("#txtResigntypRAPP > option:selected").attr("value"),
            comment: $('#txtresigRecodComments').val()
        },
        success: function (data) {
            if (data == "Success") {
                $('#RAPPAppPopup').modal('hide');
                $('#resigapp_table').trigger('reloadGrid');
                show_suc_alert_js('Successfully resignation rejected!');
                window.location.href = '/MSS/resignApproval';
            }
        },
        error: function (data) {
            show_err_alert_js('Found some error! Please try again');
        }
    });
});
