﻿//campus on change events
$("#txtCampusATR").change(function () {
    $.ajax({
        type: "POST",
        url: encodeURI("../Administration/getEmployeeJson"),
        data: { campusId: $("#txtCampusATR > option:selected").attr("value") },
        success: function (data) {
            var items = [];
            items.push("<option value=''>Employee</option>");
            $.each(data, function () {
                items.push("<option value=" + this.Value + ">" + this.Text + "</option>");
            });
            $("#txtEmployeeATR").html(items.join(' '));
        }
    });
    $.ajax({
        type: "POST",
        url: encodeURI("../Administration/getClassJson"),
        data: { campusId: $("#txtCampusATR > option:selected").attr("value") },
        success: function (data) {
            var items = [];
            items.push("<option value =''>Class/Level</option>");
            $.each(data, function () {
                items.push("<option value=" + this.Value + ">" + this.Text + "</option>");
            });
            $("#txtClassATR").html(items.join(' '));
        }
    });
    return false;
});

//class on change event
$("#txtClassATR").change(function () {
    $.ajax({
        type: "POST",
        url: encodeURI("../Administration/getSectionJson"),
        data: { campusId: $("#txtCampusATR > option:selected").attr("value"), classId: $("#txtClassATR > option:selected").attr("value") },
        success: function (data) {
            var items = [];
            items.push("<option value =''>Section</option>");
            $.each(data, function () {
                items.push("<option value=" + this.Value + ">" + this.Text + "</option>");
            });
            $("#txtSectionATR").html(items.join(' '));
        }
    });
    return false;
});

//section on change event
$("#txtSectionATR").change(function () {
    $.ajax({
        type: "POST",
        url: encodeURI("../Administration/getSubjectJson"),
        data: {
            campusId: $("#txtCampusATR > option:selected").attr("value"),
            classId: $("#txtClassATR > option:selected").attr("value"),
            sectionId: $("#txtSectionATR > option:selected").attr("value")
        },
        success: function (data) {
            var items = [];
            items.push("<option value =''>Subject</option>");
            $.each(data, function () {
                items.push("<option value=" + this.Value + ">" + this.Text + "</option>");
            });
            $("#txtSubjectATR").html(items.join(' '));
        }
    });
    return false;
});

//fetching record if its in emp0710
$("#teacherRespform").submit(function (e) {

    $.ajax({
        url: encodeURI("../Administration/getJQGridJsonATR"),
        data: {
            campusId: $("#txtCampusATR > option:selected").attr("value"),
            empId: $("#txtEmployeeATR > option:selected").attr("value")
        },
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            jQuery('#TeacherResp_table').jqGrid('clearGridData');
            $('#TeacherResp_table').jqGrid('setGridParam', { data: data });
            $("#TeacherResp_div").show();
            $("#TeacherResp_table").jqGrid('setGridState', 'visible');//or 'hidden' 

            var $screensize = $("#TeacherResp_div").width() - 42;
            var $size = ($screensize / 100);

            $("#TeacherResp_table").jqGrid({
                datatype: "local",
                loadonce: true,
                colNames: ['Action', 'Campus', 'Class', 'Section', 'Subject', 'Class Teacher Indicator', 'Campus Id', 'Class Id', 'Section Id', 'Subject Id'],
                colModel: [
                    { name: 'status', index: 'status', editable: false, width: $size * 10, align: 'center', resizable: false },
                    { name: 'campus', index: 'campus', editable: false, width: $size * 20, align: 'center', resizable: false },
                    { name: 'classes', index: 'classes', editable: false, width: $size * 15, align: 'center', resizable: false },
                    { name: 'section', index: 'section', editable: false, width: $size * 15, align: 'center', resizable: false },
                    { name: 'subject', index: 'subject', editable: false, width: $size * 20, align: 'center', resizable: false },
                    { name: 'classteach', index: 'classteach', editable: false, width: $size * 25, align: 'center', resizable: false },
                    { name: 'campusid', index: 'campusid', editable: false, width: $size * 1, align: 'center', resizable: false, hidden: true },
                    { name: 'classid', index: 'classid', editable: false, width: $size * 1, align: 'center', resizable: false, hidden: true },
                    { name: 'sectionid', index: 'sectionid', editable: false, width: $size * 1, align: 'center', resizable: false, hidden: true },
                    { name: 'subjectid', index: 'subjectid', editable: false, width: $size * 1, align: 'center', resizable: false, hidden: true }

                ],
                gridComplete: function () {
                    var ids = jQuery("#TeacherResp_table").getDataIDs();
                    for (var i = 0; i < ids.length; i++) {
                        var cl = ids[i];
                        Delete = "<i style='margin-left:15%; color:#e60000;' title='Delete' class='fa fa-trash-o fa-lg' onclick='getDeleted_ATR()'></i>";
                        jQuery("#TeacherResp_table").setRowData(ids[i], { status: Delete });
                    }
                },
                data: data,
                styleUI: 'Bootstrap',
                mtype: 'GET',
                height: 250,
                autoheight: true,
                autowidth: true,
                //rowList: [10, 20, 30],
                viewrecords: true,
                sortorder: "desc",
                threeStateSort: true,
                sortIconsBeforeText: true,
                headertitles: true,
                caption: "Teacher Responsibility",
                pager: "#TeacherResp_pager",
                viewrecords: true,
                hidegrid: false,
                shrinkToFit: false
            }).trigger('reloadGrid', [{ current: true }]);
        }
    });
    return false
    // Add selection
    $("#TeacherResp_table").setSelection(4, true);

    // Add responsive to jqGrid
    $(window).bind('resize', function () {
        var width = $('.TeacherResp_wrapper').width();

        $('#TeacherResp_table').setGridWidth(width, false);
    });

});

//deleting row
function getDeleted_ATR() {
    var selRowId = $('#TeacherResp_table').jqGrid('getGridParam', 'selrow');
    var serialId = $('#TeacherResp_table').jqGrid('getCell', selRowId, 'campus');
    $('#TeacherResp_table').delRowData(selRowId[0]);
}

// Adding row to jqgrid
$("#btnaddATR").click(function (e) {
    var classteachind = " ";

    if ($("#classteachind").prop("checked") == true)
        classteachind = "X";
    if ($("#txtClassATR > option:selected").attr("value") == ""
        && $("#txtSectionATR > option:selected").attr("value") == ""
        && $("#txtSubjectATR > option:selected").attr("value") == "") {

        var myData = [{
            "status": "<i style='margin-left:15%; color:#e60000;' title='Delete' class='fa fa-trash-o fa-lg' onclick='getDeleted_ATR()'></i>",
            "campus": "" + $("#txtCampusATR > option:selected").text(),
            'classes': " ",
            'section': " ",
            'subject': " ",
            'classteach': "" + classteachind,
            'campusid': "" + $("#txtCampusATR > option:selected").attr("value"),
            'classid': "" + $("#txtClassATR > option:selected").attr("value"),
            'sectionid': "" + $("#txtSectionATR > option:selected").attr("value"),
            'subjectid': "" + $("#txtSubjectATR > option:selected").attr("value")
        }];
    }
    else {
        var myData = [{
            "status": "<i style='margin-left:15%; color:#e60000;' title='Delete' class='fa fa-trash-o fa-lg' onclick='getDeleted_ATR()'></i>",
            "campus": "" + $("#txtCampusATR > option:selected").text(),
            'classes': "" + $("#txtClassATR > option:selected").text(),
            'section': "" + $("#txtSectionATR > option:selected").text(),
            'subject': "" + $("#txtSubjectATR > option:selected").text(),
            'classteach': "" + classteachind,
            'campusid': "" + $("#txtCampusATR > option:selected").attr("value"),
            'classid': "" + $("#txtClassATR > option:selected").attr("value"),
            'sectionid': "" + $("#txtSectionATR > option:selected").attr("value"),
            'subjectid': "" + $("#txtSubjectATR > option:selected").attr("value")
        }];
    }


    var rows = $("#TeacherResp_table").getDataIDs();
    var gridRows = $("#TeacherResp_table").jqGrid('getRowData');
    var matched = false;
    for (var i = 0; i < gridRows.length; i++) {
        if (gridRows[i].campusid == $("#txtCampusATR > option:selected").attr("value")
            && gridRows[i].classid == $("#txtClassATR > option:selected").attr("value")
            && gridRows[i].sectionid == $("#txtSectionATR > option:selected").attr("value")
            && gridRows[i].subjectid == $("#txtSubjectATR > option:selected").attr("value")) {
            matched = true;
        }
    }
    if (rows.length == [])
        rows.length = 0;
    if (matched == false) {
        $("#TeacherResp_table").jqGrid("addRowData", rows.length + 1, myData[0], "last");
        $("#TeacherResp_table").trigger("reloadGrid");
    }
    else {
        show_err_alert_js('Duplicate records!');
    }

});

//Inserting Teacher Responsibility
$("#btninsertATR").click(function (e) {
    e.preventDefault();

    var gridRows = $("#TeacherResp_table").jqGrid('getRowData');
    var rowData = new Array();
    waitingDialog.show('Please Wait: This May Take a While');

    var campusid = new Array();
    var classid = new Array();
    var sectionid = new Array();
    var subjectid = new Array();
    var classteachind = new Array();

    for (var i = 0; i < gridRows.length; i++) {
        campusid[i] = gridRows[i].campusid;
        classid[i] = gridRows[i].classid;
        sectionid[i] = gridRows[i].sectionid;
        subjectid[i] = gridRows[i].subjectid;
        classteachind[i] = gridRows[i].classteach;
    }

    $.ajax({
        type: "POST",
        url: encodeURI("../Administration/TeachRespSubmit"),
        data: {
            empid: $("#txtEmployeeATR > option:selected").attr("value"),
            campusid: campusid,
            classid: classid,
            sectionid: sectionid,
            subjectid: subjectid,
            classteachind: classteachind
        },
        success: function (data) {

            if (data == "Success") {
                waitingDialog.hide();
                show_suc_alert_js('Teacher Responsibility Successfully Updated');
                $("#TeacherResp_div").css("display", "none");
            }
            else if (data == "Error") {
                waitingDialog.hide();
                show_err_alert_js('Found Some Error! Please Try Again');
            }
        },
        dataType: "json"
    });

});