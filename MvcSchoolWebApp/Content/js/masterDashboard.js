﻿// Revenue Chart by each Campuses
$(document).ready(function () {
    revenueGraph();
});


function revenueGraph() {
    $('#revenue-chart').replaceWith('<canvas id="revenue-chart" height="120"></canvas>');
    var ctx = document.getElementById('revenue-chart');
    if (ctx != null) {
        var randomColorGenerator = function () {
            return '#' + (Math.random().toString(16) + '0000000').slice(2, 8);
        };
        var color = [];
        color.push(randomColorGenerator());
        color.push(randomColorGenerator());
        color.push(randomColorGenerator());
        color.push(randomColorGenerator());
        color.push(randomColorGenerator());
        color.push(randomColorGenerator());
        color.push(randomColorGenerator());
        color.push(randomColorGenerator());
        color.push(randomColorGenerator());
        color.push(randomColorGenerator());
        color.push(randomColorGenerator());
        color.push(randomColorGenerator());

        var data = getRevenue();
        var chart = ctx.getContext('2d')
        var myChart = new Chart(chart, {

            type: 'bar',

            data: {
                labels: data.revenuemonth,

                datasets: [
                    {
                        label: 'Revenue by Campuses',
                        data: data.revenue,
                        backgroundColor: color,
                        //backgroundColor:"black",
                        fill: false
                    },


                ]
            },

        });
    }
}
function getRevenue() {
    
    var revenue = [];
    var revenuemonth = [];

    $.ajax({
        url: encodeURI("../dashboard/getRevenue"),
        dataType: 'json',
        data: {
            campusid: $("#txtCampusAD-2 > option:selected").attr("value"),
            year: $("#txtYearAD-2").val()
        },
        async: false
    }).done(function (data) {
        data.forEach(function (obj) {
            revenue.push(obj.revenue);
            revenuemonth.push(obj.revenuemonth);
        });
    });
    return {
        revenue: revenue,
        revenuemonth: revenuemonth
    };
}

$("#txtCampusAD-2").change(function () {
    revenueGraph();
});


$("#txtYearAD-2").change(function () {
    revenueGraph();
});

// Revenue Chart by each Campuses

//Bar Chart
var bchart = document.getElementById("barchartContainer");

//Bar-chart No of Admission per Campuses
if (bchart != null) {
    var chart = new CanvasJS.Chart("barchartContainer", {
        animationEnabled: true,
        axisY: {
            title: "Current Strength",
            titleFontColor: "#4F81BC",
            lineColor: "#4F81BC",
            labelFontColor: "#4F81BC",
            tickColor: "#4F81BC"
        },
        axisY2: {
            title: "Current Capacity",
            titleFontColor: "#C0504E",
            lineColor: "#C0504E",
            labelFontColor: "#C0504E",
            tickColor: "#C0504E"
        },
        toolTip: {
            shared: true
        },
        legend: {
            cursor: "pointer",
            itemclick: toggleDataSeries
        },
        data: [{
            type: "column",
            name: "Current Strength",
            legendText: "Current Strength",
            showInLegend: true,
            dataPoints: [
                { label: "Campus-1", y: 266.21 },
                { label: "Campus-2", y: 302.25 },
                { label: "Campus-3", y: 157.20 },
                { label: "Campus-4", y: 148.77 },
                { label: "Campus-5", y: 101.50 },
                { label: "Campus-6", y: 97.8 }
            ]
        },
        {
            type: "column",
            name: "Current Capacity",
            legendText: "Current Capacity",
            axisYType: "secondary",
            showInLegend: true,
            dataPoints: [
                { label: "Campus-1", y: 10.46 },
                { label: "Campus-2", y: 2.27 },
                { label: "Campus-3", y: 3.99 },
                { label: "Campus-4", y: 4.45 },
                { label: "Campus-5", y: 2.92 },
                { label: "Campus-6", y: 3.1 }
            ]
        }]
    });
    chart.render();
}
function toggleDataSeries(e) {
    if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
        e.dataSeries.visible = false;
    }
    else {
        e.dataSeries.visible = true;
    }
    chart.render();
}

//Bar Chart Average Test Marks
var dchart = document.getElementById("admission-overall");

//Doughnut-chart No of Admission per Campuses
if (dchart != null) {
    new Chart(dchart, {
        type: 'doughnut',
        data: {
            labels: ["Number of Admission"],
            datasets: [
                {
                    label: "",
                    backgroundColor: ["#3e95cd"],
                    data: [365]
                }
            ]
        },
        options: {
            title: {
                display: true,
                responsive: true,
                maintainAspectRatio: false
            }
        }
    });
}